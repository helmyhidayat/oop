<?php

// TUGAS 01

require 'Animal.php';


$sheep = new Animal('Shaun');

echo $sheep->name . '<br>'; // "shaun"
echo $sheep->legs . '<br>'; // 4
echo $sheep->cold_blooded . '<br>'; // "no"

echo '<br><hr><br>';
// TUGAS 02
require 'Ape.php';
require 'Frog.php';

$sungokong = new Ape('Kera Sakti');
echo $sungokong->name . '<br>';
echo $sungokong->legs . '<br>';
echo $sungokong->cold_blooded . '<br>';
echo $sungokong->yell() . '<br>';

echo '<br><hr><br>';

$kodok = new Frog('Buduk');
echo $kodok->name . '<br>';
echo $kodok->legs . '<br>';
echo $kodok->cold_blooded . '<br>';
echo $kodok->jump() . '<br>';
?>